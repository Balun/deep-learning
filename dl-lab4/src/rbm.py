"""
"""
import math
import pickle

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from src.utils import tile_raster_images


class RBM:
    """
    """

    def __init__(self, h1_shape, v_shape, n_u=5000, gibbs=1, alpha=0.1):
        """

        :param h1_shape:
        :param v_shape:
        :param n_u:
        :param gibbs:
        :param alpha:
        """
        self.h1_shape = h1_shape
        self.v_shape = v_shape
        self.n_u = 5000
        self.gibbs = 1
        self.alpha = 0.1

        self.N_v = v_shape[0] * v_shape[1]
        self.N_h = h1_shape[0] * h1_shape[1]

        self.X1 = tf.placeholder("float", [None, self.N_v])
        self.w1 = weights([self.N_v, self.N_h])
        self.v_b1 = bias([self.N_v])
        self.h_b1 = bias([self.N_h])

        h0_prob = tf.nn.sigmoid(tf.matmul(self.X1, self.w1) +
                                self.h_b1)
        self.h0 = sample_prob(h0_prob)
        self.h1 = self.h0

        for step in range(gibbs):
            self.v1_prob = tf.nn.sigmoid(tf.matmul(self.h1, self.w1,
                                                   transpose_b=True) +
                                         self.v_b1)
            self.v1 = sample_prob(self.v1_prob)

            self.h1_prob = tf.nn.sigmoid(tf.matmul(self.v1, self.w1) +
                                         self.h_b1)
            self.h1 = sample_prob(self.h1_prob)

        # positive phase
        w1_positive_grad = tf.matmul(self.X1, self.h0, transpose_a=True)

        # negative phase
        w1_negative_grad = tf.matmul(self.v1, self.h1, transpose_a=True)

        dw1 = (w1_positive_grad - w1_negative_grad) / tf.to_float(
            tf.shape(self.X1)[0])

        update_w1 = tf.assign_add(self.w1, alpha * dw1)
        update_vb1 = tf.assign_add(self.v_b1, alpha * tf.reduce_mean(
            self.X1 -
            self.v1, 0))
        update_hb1 = tf.assign_add(self.h_b1, alpha * tf.reduce_mean(
            self.h0 - self.h1, 0))

        self.out1 = (update_w1, update_vb1, update_hb1)

        self.v1_prob = tf.nn.sigmoid(tf.matmul(self.h1, self.w1,
                                               transpose_b=True) + self.v_b1)
        self.v1 = sample_prob(self.v1_prob)

        self.err1 = self.X1 - self.v1_prob
        self.err_sum1 = tf.reduce_mean(self.err1 * self.err1)

        self.session = tf.Session()

    def fit(self, dataset, n_samples, batch_size=100, epochs=100, trace=False):
        errors = []
        total_batch = int(n_samples / batch_size) * epochs

        self.session.run(tf.global_variables_initializer())

        for i in range(total_batch):
            batch, label = dataset.train.next_batch(batch_size)
            err, _ = self.session.run([self.err_sum1, self.out1], feed_dict={
                self.X1: batch})

            if i % (int(total_batch / 10)) == 0:
                errors.append(err)

                if trace:
                    print(i, err)

        return np.array(errors)

    def eval(self, X_test, N_u):
        w1s = self.w1.eval(session=self.session)
        v_b1s = self.v_b1.eval(session=self.session)
        h_b1s = self.h_b1.eval(session=self.session)
        v_r, h1s = self.session.run([self.v1_prob, self.h1], feed_dict={
            self.X1: X_test[0:N_u, :]})

        return w1s, v_b1s, h_b1s, v_r, h1s

    def save_model(self, file_path):
        """

        :param file_path:
        :return:
        """
        w1s = self.w1.eval(session=self.session)
        vb1s = self.v_b1.eval(session=self.session)
        hb1s = self.h_b1.eval(session=self.session)
        pickle.dump((w1s, vb1s, hb1s), open(file_path, 'wb'))


def weights(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias(shape):
    initial = tf.zeros(shape, dtype=tf.float32)
    return tf.Variable(initial)


def sample_prob(probs):
    """Uzorkovanje vektora x prema vektoru vjerojatnosti p(x=1) = probs"""
    return tf.to_float(tf.random_uniform(tf.shape(probs)) <= probs)


def draw_weights(W, shape, N, stat_shape, interpolation="bilinear"):
    """Vizualizacija težina
    W -- vektori težina
    shape -- tuple dimenzije za 2D prikaz težina - obično dimenzije ulazne slike, npr. (28,28)
    N -- broj vektora težina
    shape_state -- dimezije za 2D prikaz stanja (npr. za 100 stanja (10,10)
    """
    image = (tile_raster_images(
        X=W.T,
        img_shape=shape,
        tile_shape=(int(math.ceil(N / stat_shape[0])), stat_shape[0]),
        tile_spacing=(1, 1)))
    plt.figure(figsize=(10, 14))
    plt.imshow(image, interpolation=interpolation)
    plt.axis('off')


def draw_reconstructions(ins, outs, states, shape_in, shape_state, N):
    """Vizualizacija ulaza i pripadajućih rekonstrukcija i stanja skrivenog sloja
    ins -- ualzni vektori
    outs -- rekonstruirani vektori
    states -- vektori stanja skrivenog sloja
    shape_in -- dimezije ulaznih slika npr. (28,28)
    shape_state -- dimezije za 2D prikaz stanja (npr. za 100 stanja (10,10)
    N -- broj uzoraka
    """
    plt.figure(figsize=(8, int(2 * N)))
    for i in range(N):
        plt.subplot(N, 4, 4 * i + 1)
        plt.imshow(ins[i].reshape(shape_in), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("Test input")
        plt.axis('off')
        plt.subplot(N, 4, 4 * i + 2)
        plt.imshow(outs[i][0:784].reshape(shape_in), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("Reconstruction")
        plt.axis('off')
        plt.subplot(N, 4, 4 * i + 3)
        plt.imshow(states[i].reshape(shape_state), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("States")
        plt.axis('off')
    plt.tight_layout()


def draw_generated(stin, stout, gen, shape_gen, shape_state, N):
    """Vizualizacija zadanih skrivenih stanja, konačnih skrivenih stanja i pripadajućih rekonstrukcija
    stin -- početni skriveni sloj
    stout -- rekonstruirani vektori
    gen -- vektori stanja skrivenog sloja
    shape_gen -- dimezije ulaznih slika npr. (28,28)
    shape_state -- dimezije za 2D prikaz stanja (npr. za 100 stanja (10,10)
    N -- broj uzoraka
    """
    plt.figure(figsize=(8, int(2 * N)))
    for i in range(N):
        plt.subplot(N, 4, 4 * i + 1)
        plt.imshow(stin[i].reshape(shape_state), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("set state")
        plt.axis('off')
        plt.subplot(N, 4, 4 * i + 2)
        plt.imshow(stout[i][0:784].reshape(shape_state), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("final state")
        plt.axis('off')
        plt.subplot(N, 4, 4 * i + 3)
        plt.imshow(gen[i].reshape(shape_gen), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("generated visible")
        plt.axis('off')
    plt.tight_layout()
