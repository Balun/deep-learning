"""
"""
from tensorflow.keras import Sequential

import tensorflow as tf
import tensorflow.keras.layers as layers
import tensorflow.keras.optimizers as optimizers

import matplotlib.pyplot as plt

from IPython import display

import os


class GAN:
    """
    """

    def __init__(self, batch_size=100, learning_rate=0.0002):
        self.batch_size = batch_size

        self.discriminator = get_descriminator()
        self.generator = get_generator()

        self.desc_opt = optimizers.Adam(learning_rate)
        self.gen_opt = optimizers.Adam(learning_rate)

        """
        checkpoint_dir = 'res/models/gan/train'
        self.checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
        self.checkpoint = tf.train.Checkpoint(
            generator_optimizer=self.gen_opt,
            discriminator_optimizer=self.desc_opt,
            generator=self.generator,
            discriminator=self.discriminator)
        """

    def fit(self, dataset, seed, noise_dim=100, epochs=20):
        for epoch in range(epochs):
            for image_batch in dataset:
                self.step(image_batch, noise_dim)

            # Produce images for the GIF as we go
            display.clear_output(wait=True)
            generate_and_save_images(self.generator,
                                     epoch + 1,
                                     seed)

            """
            # Save the model every 5 epochs
            if (epoch + 1) % 15 == 0:
                self.checkpoint.save(file_prefix=self.checkpoint_prefix)
            """

        # Generate after the final epoch
        display.clear_output(wait=True)
        generate_and_save_images(self.generator,
                                 epochs,
                                 seed)

    def step(self, images, noise_dim=100):
        noise = tf.random_normal([self.batch_size, noise_dim])

        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            generated_images = self.generator(noise, training=True)

            real_output = self.discriminator(images, training=True)
            fake_output = self.discriminator(generated_images, training=True)

            gen_loss = generator_loss(fake_output)
            disc_loss = discriminator_loss(real_output, fake_output)

        gradients_of_generator = gen_tape.gradient(gen_loss,
                                                   self.generator.trainable_variables)
        gradients_of_discriminator = disc_tape.gradient(disc_loss,
                                                        self.discriminator.trainable_variables)

        self.gen_opt.apply_gradients(
            zip(gradients_of_generator, self.generator.trainable_variables))
        self.gen_opt.apply_gradients(
            zip(gradients_of_discriminator,
                self.discriminator.trainable_variables))

    def save_checkpint(self, dir):
        checkpoint_dir = './training_checkpoints'
        checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
        checkpoint = tf.train.Checkpoint(
            generator_optimizer=self.gen_opt,
            discriminator_optimizer=self.desc_opt,
            generator=self.generator,
            discriminator=self.discriminator)

        # TODO


def get_generator(batch_norm=True):
    model = Sequential()
    model.add(layers.Dense(8 * 8 * 256, use_bias=False, input_shape=(100,)))
    if batch_norm:
        model.add(layers.BatchNormalization())
    model.add(layers.LeakyReLU())

    model.add(layers.Reshape((8, 8, 256)))
    assert model.output_shape == (
        None, 8, 8, 256)  # Note: None is the batch size

    model.add(
        layers.Conv2DTranspose(128, (5, 5), strides=(1, 1), padding='same',
                               use_bias=False))
    assert model.output_shape == (None, 8, 8, 128)
    if batch_norm:
        model.add(layers.BatchNormalization())
    model.add(layers.LeakyReLU())

    model.add(
        layers.Conv2DTranspose(64, (5, 5), strides=(2, 2), padding='same',
                               use_bias=False))
    assert model.output_shape == (None, 16, 16, 64)
    if batch_norm:
        model.add(layers.BatchNormalization())
    model.add(layers.LeakyReLU())

    model.add(layers.Conv2DTranspose(1, (5, 5), strides=(2, 2), padding='same',
                                     use_bias=False, activation='tanh'))
    assert model.output_shape == (None, 32, 32, 1)

    return model


def get_descriminator():
    model = Sequential()
    model.add(layers.Conv2D(64, (5, 5), strides=(2, 2), padding='same',
                            input_shape=[32, 32, 1]))
    model.add(layers.LeakyReLU())
    model.add(layers.Dropout(0.3))

    model.add(layers.Conv2D(128, (5, 5), strides=(2, 2), padding='same'))
    model.add(layers.LeakyReLU())
    model.add(layers.Dropout(0.3))

    model.add(layers.Flatten())
    model.add(layers.Dense(1))

    return model


def discriminator_loss(real_output, fake_output):
    cross_entropy = tf.keras.losses.binary_crossentropy

    real_loss = cross_entropy(tf.ones_like(real_output), real_output)
    fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
    total_loss = real_loss + fake_loss
    return total_loss


def generator_loss(fake_output):
    cross_entropy = tf.keras.losses.binary_crossentropy

    return cross_entropy(tf.ones_like(fake_output), fake_output)


def generate_and_save_images(model, epoch, test_input):
    # Notice `training` is set to False.
    # This is so all layers run in inference mode (batchnorm).
    predictions = model(test_input, training=False)

    fig = plt.figure(figsize=(4, 4))

    for i in range(predictions.shape[0]):
        plt.subplot(4, 4, i + 1)
        plt.imshow(predictions[i, :, :, 0] * 127.5 + 127.5, cmap='gray')
        plt.axis('off')

    plt.savefig('image_at_epoch_{:04d}.png'.format(epoch))
    plt.show()
