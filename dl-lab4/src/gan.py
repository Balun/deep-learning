"""
"""
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

import src.utils as utils

import math


class GAN:
    """
    """

    def __init__(self, input_size, noise_dim=100, batch_size=100,
                 learning_rate=0.0002):
        """

        :param input_size:
        :param batch_size:
        :param learning_rate:
        """
        self.input_size = input_size
        self.batch_size = batch_size
        self.leaning_rate = learning_rate
        self.noise_dim = noise_dim

        tf.reset_default_graph()

        self.z = tf.placeholder(tf.float32, shape=[None, 1, 1, 100])
        self.x = tf.placeholder(tf.float32, shape=[None, 32, 32, 1])
        self.is_train = tf.placeholder(dtype=tf.bool)

        self.generator = build_generator(self.z, self.is_train)

        real_desc, real_desc_logits = build_single_descriminator(self.x)
        fake_desc, fake_desc_logits = build_single_descriminator(
            self.generator, reuse=True)

        # labels for learning
        true_labels = tf.ones([batch_size, 1, 1, 1])
        fake_labels = tf.zeros([batch_size, 1, 1, 1])

        d_loss_real = tf.reduce_mean(
            tf.nn.sparse_softmax_cross_entropy_with_logits(
                logits=real_desc_logits, labels=true_labels))
        d_loss_fake = tf.reduce_mean(
            tf.nn.sparse_softmax_cross_entropy_with_logits(
                logits=fake_desc_logits, labels=fake_labels))

        self.d_loss = d_loss_real + d_loss_fake

        self.g_loss = tf.reduce_mean(
            tf.nn.sparse_softmax_cross_entropy_with_logits(
                logits=build_single_descriminator(self.generator),
                labels=true_labels))

        # trainable variables for each network
        t_vars = tf.trainable_variables()
        d_vars = [var for var in t_vars if
                  var.name.startswith('discriminator')]
        g_vars = [var for var in t_vars if var.name.startswith('generator')]

        with tf.control_dependencies(
                tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
            self.d_optim = tf.train.AdamOptimizer(learning_rate,
                                                  beta1=0.3).minimize(
                self.d_loss,
                var_list=d_vars)
            self.g_optim = tf.train.AdamOptimizer(learning_rate,
                                                  beta1=0.3).minimize(
                self.g_loss,
                var_list=g_vars)

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True

        self.session = tf.InteractiveSession(config=config)

    def fit(self, dataset, n_samples, n_epochs=20, trace=False):
        """

        :param dataset:
        :param n_samples:
        :param n_epochs:
        :param trace:
        :return:
        """
        self.session.run(tf.global_variables_initializer())
        fixed_z_ = gen_z(100, 100)
        total_batch = int(n_samples / self.batch_size)
        step = 0

        for epoch in range(n_epochs):
            for iter in range(total_batch):
                # update discriminator
                x_ = dataset[iter * self.batch_size:(iter + 1) *
                                                    self.batch_size]

                # update discriminator

                z_ = gen_z(100, self.batch_size)
                loss_d_, _ = self.session.run([self.d_loss, self.d_optim],
                                              {self.x: x_, self.z: z_,
                                               self.is_train:
                                                   True})
                # update generator
                loss_g_, _ = self.session.run([self.g_loss, self.g_optim],
                                              {self.x: x_, self.z: z_,
                                               self.is_train:
                                                   True})

            print('[%d/%d] loss_d: %.3f, loss_g: %.3f' % (
                (epoch + 1), n_epochs, loss_d_, loss_g_))

            test_images = self.session.run(self.generator, {self.z: fixed_z_,
                                                            self.is_train:
                                                                False})
            show_generated(test_images, 100)


def leaky_relu(x, th=0.2):
    return tf.maximum(th * x, x)


def build_single_descriminator(x, reuse=False, is_train=True):
    """

    :param x:
    :param reuse:
    :return:
    """
    with tf.variable_scope('discriminator', reuse=reuse):
        # 1st hidden layer
        conv = tf.layers.conv2d(x, 64, [4, 4], strides=(2, 2),
                                padding='same', activation=leaky_relu)

        # 2nd hidden layer
        conv = tf.layers.conv2d(conv, 128, [4, 4], strides=(2, 2),
                                padding='same', activation=leaky_relu)

        # output layer
        conv = tf.layers.conv2d(conv, 1, [4, 4], strides=(2, 2),
                                padding='same', activation=leaky_relu)

        out = tf.nn.sigmoid(conv)

        return out, conv


def build_generator(x, is_train=True):
    """

    :param z:
    :param reuse:
    :return:
    """
    # 1st hidden layer
    conv = tf.layers.conv2d_transpose(x, 512, [4, 4], strides=(1, 1),
                                      padding='valid')
    lrelu_ = leaky_relu(tf.layers.batch_normalization(conv, training=is_train))

    # 2nd hidden layer
    conv = tf.layers.conv2d_transpose(lrelu_, 256, [4, 4], strides=(1, 1),
                                      padding='valid')
    lrelu_ = leaky_relu(tf.layers.batch_normalization(conv, training=is_train))

    # 3rd hidden layer
    conv = tf.layers.conv2d_transpose(lrelu_, 128, [4, 4], strides=(1, 1),
                                      padding='valid')
    lrelu_ = leaky_relu(tf.layers.batch_normalization(conv, training=is_train))

    # output layer
    conv = tf.layers.conv2d_transpose(lrelu_, 1, [4, 4], strides=(1, 1),
                                      padding='valid')
    lrelu_ = leaky_relu(tf.layers.batch_normalization(conv, training=is_train))

    return lrelu_


def show_generated(G, N, shape=(32, 32), stat_shape=(10, 10),
                   interpolation="bilinear"):
    """Visualization of generated samples
     G - generated samples
     N - number of samples
     shape - dimensions of samples eg (32,32)
     stat_shape - dimension for 2D sample display (eg for 100 samples (10,10)
    """

    image = (utils.tile_raster_images(
        X=G,
        img_shape=shape,
        tile_shape=(int(math.ceil(N / stat_shape[0])), stat_shape[0]),
        tile_spacing=(1, 1)))
    plt.figure(figsize=(10, 14))
    plt.imshow(image, interpolation=interpolation)
    plt.axis('off')
    plt.show()


def gen_z(N, batch_size):
    z = np.random.normal(0, 1, (batch_size, 1, 1, N))
    return z
