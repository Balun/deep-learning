"""
"""
import tensorflow as tf
import matplotlib.pyplot as plt

import pickle
import math

import src.utils as utils


class DBN:
    """
    """

    def __init__(self, rbm_weights_path, N_v, N_h2, h2_shape, gibbs=2,
                 alpha=0.1):
        self.gibbs = gibbs
        self.alpha = alpha

        self.w1s, self.v_b1s, self.h_b1s = pickle.load(open(rbm_weights_path,
                                                            "rb"))

        self.N_h2 = N_h2
        self.N_v = N_v
        self.h2_shape = h2_shape

        self.X2 = tf.placeholder('float', [None, self.N_v])
        self.w1a = tf.Variable(self.w1s)
        self.vb1a = tf.Variable(self.v_b1s)
        self.hb1a = tf.Variable(self.h_b1s)
        self.w2 = weights([N_h2, N_h2])
        self.v_b2 = bias([N_h2])
        self.h_b2 = bias([N_h2])

        # visible layer of the second RBM
        v2_prob = tf.nn.sigmoid(tf.matmul(self.X2, self.w1a) + self.hb1a)
        v2 = sample_prob(v2_prob)
        # hidden layer of the second RBM
        h2_prob = tf.nn.sigmoid(tf.matmul(v2, self.w2) + self.h_b2)
        h2 = sample_prob(h2_prob)
        h3 = h2

        for step in range(gibbs):
            v3_prob = tf.nn.sigmoid(tf.matmul(h3, self.w2, transpose_b=True) +
                                    self.v_b2)
            v3 = sample_prob(v3_prob)
            self.h3_prob = tf.nn.sigmoid(tf.matmul(v3, self.w2) + self.h_b2)
            self.h3 = sample_prob(self.h3_prob)

        w2_positive_grad = tf.matmul(v2, h2, transpose_a=True)
        w2_negative_grad = tf.matmul(v3, h3, transpose_a=True)

        dw2 = (w2_positive_grad - w2_negative_grad) / tf.to_float(
            tf.shape(v2)[0])

        update_w2 = tf.assign_add(self.w2, alpha * dw2)
        update_vb2 = tf.assign_add(self.v_b2, alpha * tf.reduce_mean(v2 - v3,
                                                                     0))
        update_hb2 = tf.assign_add(self.h_b2, alpha * tf.reduce_mean(h2 - h3,
                                                                     0))

        self.out2 = (update_w2, update_vb2, update_hb2)

        # input reconstruction by top layer state vector h3
        v4_prob = tf.nn.sigmoid(tf.matmul(h3, self.w2, transpose_b=True) +
                                self.hb1a)
        v4 = sample_prob(v4_prob)
        self.v5_prob = tf.nn.sigmoid(tf.matmul(v4, self.w1a,
                                               transpose_b=True) +
                                     self.vb1a)

        err2 = self.X2 - self.v5_prob
        self.err_sum2 = tf.reduce_mean(err2 * err2)

        self.session = tf.Session()

    def fit_and_eval(self, dataset, n_samples, X_test, batch_size=100,
                     epochs=100,
                     trace=False, weights_path=None):
        self.session.run(tf.global_variables_initializer())
        total_batch = int(n_samples / batch_size) * epochs
        errors = []

        for i in range(total_batch):
            batch, label = dataset.train.next_batch(batch_size)
            err, _ = self.session.run([self.err_sum2, self.out2], feed_dict={
                self.X2: batch})

            if i % (int(total_batch / 10)) == 0:
                errors.append(err)

                if trace:
                    print("Batch count: ", i, "  Avg. reconstruction error: ",
                          err)

        w2s, vb2s, hb2s = self.session.run([self.w2, self.v_b2, self.h_b2],
                                           feed_dict={self.X2: batch})
        vr2, h3_probs, h3s = self.session.run([self.v5_prob, self.h3_prob,
                                               self.h3],
                                              feed_dict={
                                                  self.X2: X_test[0:50, :]})

        if weights_path:
            pickle.dump((self.w1s, w2s, self.h_b1s, self.v_b1s, hb2s, vb2s),
                        open(weights_path, 'wb'))

        return vr2, h3_probs, h3s, errors


def weights(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias(shape):
    initial = tf.zeros(shape, dtype=tf.float32)
    return tf.Variable(initial)


def sample_prob(probs):
    """Sampling of vector x by probability vector p(x=1) = probs"""
    return tf.nn.relu(
        tf.sign(probs - tf.random_uniform(tf.shape(probs))))


def draw_weights(W, shape, N, stat_shape, interpolation="bilinear"):
    """Vizualizacija težina
    W -- vektori težina
    shape -- tuple dimenzije za 2D prikaz težina - obično dimenzije ulazne slike, npr. (28,28)
    N -- broj vektora težina
    shape_state -- dimezije za 2D prikaz stanja (npr. za 100 stanja (10,10)
    """
    image = (utils.tile_raster_images(
        X=W.T,
        img_shape=shape,
        tile_shape=(int(math.ceil(N / stat_shape[0])), stat_shape[0]),
        tile_spacing=(1, 1)))
    plt.figure(figsize=(10, 14))
    plt.imshow(image, interpolation=interpolation)
    plt.axis('off')


def draw_reconstructions(ins, outs, states, shape_in, shape_state, N):
    """Vizualizacija ulaza i pripadajućih rekonstrukcija i stanja skrivenog sloja
    ins -- ualzni vektori
    outs -- rekonstruirani vektori
    states -- vektori stanja skrivenog sloja
    shape_in -- dimezije ulaznih slika npr. (28,28)
    shape_state -- dimezije za 2D prikaz stanja (npr. za 100 stanja (10,10)
    N -- broj uzoraka
    """
    plt.figure(figsize=(8, int(2 * N)))
    for i in range(N):
        plt.subplot(N, 4, 4 * i + 1)
        plt.imshow(ins[i].reshape(shape_in), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("Test input")
        plt.axis('off')
        plt.subplot(N, 4, 4 * i + 2)
        plt.imshow(outs[i][0:784].reshape(shape_in), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("Reconstruction")
        plt.axis('off')
        plt.subplot(N, 4, 4 * i + 3)
        plt.imshow(states[i].reshape(shape_state), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("States")
        plt.axis('off')
    plt.tight_layout()


def draw_generated(stin, stout, gen, shape_gen, shape_state, N):
    """Vizualizacija zadanih skrivenih stanja, konačnih skrivenih stanja i pripadajućih rekonstrukcija
    stin -- početni skriveni sloj
    stout -- rekonstruirani vektori
    gen -- vektori stanja skrivenog sloja
    shape_gen -- dimezije ulaznih slika npr. (28,28)
    shape_state -- dimezije za 2D prikaz stanja (npr. za 100 stanja (10,10)
    N -- broj uzoraka
    """
    plt.figure(figsize=(8, int(2 * N)))
    for i in range(N):
        plt.subplot(N, 4, 4 * i + 1)
        plt.imshow(stin[i].reshape(shape_state), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("set state")
        plt.axis('off')
        plt.subplot(N, 4, 4 * i + 2)
        plt.imshow(stout[i][0:784].reshape(shape_state), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("final state")
        plt.axis('off')
        plt.subplot(N, 4, 4 * i + 3)
        plt.imshow(gen[i].reshape(shape_gen), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("generated visible")
        plt.axis('off')
    plt.tight_layout()
