"""
"""

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

import os


class VAE:
    """
    """

    def __init__(self, encoder_config, decoder_config, n_hidden, img_shape,
                 batch_size=100, learning_rate=0.001):
        """

        :param encoder_config:
        :param decoder_config:
        :param n_hidden:
        :param img_shape:
        """
        self.input_shape = img_shape
        self.n_features = img_shape[0] * img_shape[1]
        self.batch_size = batch_size

        tf.reset_default_graph()
        self.session = tf.InteractiveSession()

        self.x = tf.placeholder("float", [None, self.n_features])

        encoder_layers = []

        for i in range(len(encoder_config)):
            if i == 0:
                encoder_layers.append(vae_layer(self.x, self.n_features,
                                                encoder_config[i],
                                                "layer_e%d" % (i + 1)))
            else:
                encoder_layers.append(vae_layer(encoder_layers[i - 1],
                                                encoder_config[i - 1],
                                                encoder_config[i],
                                                "layer_e%d" % (i + 1)))

        with tf.name_scope('z'):
            # definirajte skrivene varijable i pripadajući generator šuma
            self.z_mean = vae_layer(encoder_layers[-1], encoder_config[-1],
                                    n_hidden,
                                    'z_mean',
                                    act=tf.identity)
            self.z_log_sigma_sq = vae_layer(encoder_layers[-1], encoder_config[
                -1], n_hidden, 'z_log_sigma_sq', act=tf.identity)
            eps = tf.random_normal((batch_size, n_hidden), 0, 1,
                                   dtype=tf.float32)

            self.z = tf.add(self.z_mean,
                            tf.multiply(tf.sqrt(tf.exp(self.z_log_sigma_sq)),
                                        eps))
            tf.summary.histogram('activations', self.z)

        decoder_layers = []

        for i in range(len(decoder_config)):
            if i == 0:
                decoder_layers.append(vae_layer(self.z, n_hidden,
                                                decoder_config[
                                                    0], 'layer_d%d' % (i + 1)))
            else:
                vae_layer(decoder_layers[i - 1], decoder_config[i - 1],
                          decoder_config[i], "layer_d%d" % (i + 1))

        # definirajte srednju vrijednost rekonstrukcije
        x_reconstr_mean = vae_layer(decoder_layers[-1], decoder_config[-1],
                                    self.n_features, 'x_reconstr_mean',
                                    act=tf.identity)

        self.x_reconstr_mean_out = tf.nn.sigmoid(x_reconstr_mean)

        # definirajte dvije komponente funkcije cijene
        with tf.name_scope('cost'):
            cost1 = tf.reduce_sum(
                tf.nn.sigmoid_cross_entropy_with_logits(logits=x_reconstr_mean,
                                                        labels=self.x), 1)
            tf.summary.histogram('cross_entropy', cost1)

            cost2 = -1 / 2 * tf.reduce_sum(
                1 + self.z_log_sigma_sq - tf.square(self.z_mean) - tf.exp(
                    self.z_log_sigma_sq), 1)
            tf.summary.histogram('D_KL', cost2)

            self.cost = tf.reduce_mean(cost1 + cost2)  # average over batch
            tf.summary.histogram('cost', self.cost)

        with tf.name_scope('train'):
            self.optimizer = tf.train.AdamOptimizer(
                learning_rate=learning_rate).minimize(self.cost)

        # Prikupljanje podataka za Tensorboard
        self.merged = tf.summary.merge_all()

        self.saver = tf.train.Saver()

    def fit(self, dataset, n_samples, n_epochs=100, train_writer=None,
            trace=False):
        self.session.run(tf.global_variables_initializer())

        total_batch = int(n_samples / self.batch_size)
        step = 0

        for epoch in range(n_epochs):
            avg_cost = 0.

            for i in range(total_batch):
                batch_xs, _ = dataset.train.next_batch(self.batch_size)
                # Fit training using batch data
                opt, cos = self.session.run((self.optimizer, self.cost),
                                            feed_dict={self.x:
                                                           batch_xs})
                # Compute average loss
                avg_cost += cos / n_samples * self.batch_size

            # Display logs per epoch step
            if (epoch % (int(n_epochs / 10)) == 0) and trace:
                print("Epoch:", '%04d' % (epoch + 1),
                      "cost=", "{:.9f}".format(avg_cost))
                run_options = tf.RunOptions(
                    trace_level=tf.RunOptions.FULL_TRACE)
                run_metadata = tf.RunMetadata()
                summary, _ = self.session.run([self.merged, self.optimizer],
                                              feed_dict={self.x: batch_xs},
                                              options=run_options,
                                              run_metadata=run_metadata)
                train_writer.add_run_metadata(run_metadata,
                                              'epoch%03d' % epoch)
                train_writer.add_summary(summary, i)

                self.saver.save(self.session, os.path.join('train',
                                                           "model.ckpt"),
                                epoch)

    def reconstruct(self, x_sample):
        return self.session.run([self.x_reconstr_mean_out, self.z],
                                feed_dict={self.x: x_sample})

    def z_mean_and_sigma(self, x_sample):
        return self.session.run((self.z_mean, self.z_log_sigma_sq),
                                feed_dict={self.x: x_sample})


def get_canvas(Z, ind, nx, ny, in_shape, batch_size, sess):
    """Crtanje rekonstrukcija na odgovarajućim pozicijama u 2D prostoru skrivenih varijabli
    Z -- skriveni vektori raspoređeni u gridu oko ishodišta
    ind -- indeksi za rezanje Z-a na batch_size blokove za slanje u graf -zbog problema sa random generatorom
    nx -- raspon grida po x osi - skrivena varijabla z0
    ny -- raspon grida po y osi - skrivena varijabla z1
    in_shape -- dimenzije jedne rekonstrukcije i.e. ulazne sličice
    batch_size -- veličina minibatcha na koji je graf naviknut
    sess -- session grafa mreže
    """
    # get reconstructions for visualiations
    X = np.empty(
        (0, in_shape[0] * in_shape[1]))  # empty array for concatenation
    # split hidden vectors into minibatches of batch_size due to TF random generator limitation
    for batch in np.array_split(Z, ind):
        # fill up last batch to full batch_size if neccessary
        # this addition will not be visualized, but is here to avoid TF error
        if batch.shape[0] < batch_size:
            batch = np.concatenate((batch, np.zeros(
                (batch_size - batch.shape[0], batch.shape[1]))), 0)
        # get batch_size reconstructions and add them to array of previous reconstructions
        X = np.vstack((X, sess.run(x_reconstr_mean_out, feed_dict={z: batch})))
    # make canvas with reconstruction tiles arranged by the hidden state coordinates of each reconstruction
    # this is achieved for all reconstructions by clever use of reshape, swapaxes and axis inversion
    return (
        X[0:nx * ny, :].reshape((nx * ny, in_shape[0], in_shape[1])).swapaxes(
            0, 1)
            .reshape((in_shape[0], ny, nx * in_shape[1])).swapaxes(0, 1)[::-1,
        :,
        :]
            .reshape((ny * in_shape[0], nx * in_shape[1])))


def draw_reconstructions(ins, outs, states, shape_in, shape_state):
    """Vizualizacija ulaza i pripadajućih rekonstrkcija i stanja skrivenog sloja
    ins -- ualzni vektori
    outs -- rekonstruirani vektori
    states -- vektori stanja skrivenog sloja
    shape_in -- dimezije ulaznih slika npr. (28,28)
    shape_state -- dimezije za 2D prikaz stanja (npr. za 100 stanja (10,10)
    """
    plt.figure(figsize=(8, 12 * 4))
    for i in range(20):
        plt.subplot(20, 4, 4 * i + 1)
        plt.imshow(ins[i].reshape(shape_in), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("Test input")
        plt.subplot(20, 4, 4 * i + 2)
        plt.imshow(outs[i][0:784].reshape(shape_in), vmin=0, vmax=1,
                   interpolation="nearest")
        plt.title("Reconstruction")
        plt.subplot(20, 4, 4 * i + 3)
        plt.imshow(states[i][0:(shape_state[0] * shape_state[1])].reshape(
            shape_state),
            vmin=-4, vmax=4, interpolation="nearest")
        plt.colorbar()
        plt.title("States")
    plt.tight_layout()


def plot_latent(inmat, labels):
    """Crtanje pozicija uzoraka u 2D latentnom prostoru
    inmat -- matrica latentnih stanja
    labels -- labela klas
    """
    plt.figure(figsize=(8, 6))
    plt.axis([-4, 4, -4, 4])
    plt.gca().set_autoscale_on(False)

    plt.scatter(inmat[:, 0], inmat[:, 1], c=np.argmax(labels, 1))
    plt.colorbar()
    plt.xlabel('z0')
    plt.ylabel('z1')


def save_latent_plot(name):
    """Spremanje trenutnog figure-a
    name -- ime datoteke
    """
    plt.savefig(name)


def weight_variable(shape, name):
    """Kreiranje težina"""
    # http://andyljones.tumblr.com/post/110998971763/an-explanation-of-xavier-initialization
    return tf.get_variable(name, shape=shape,
                           initializer=tf.contrib.layers.xavier_initializer())


def bias_variable(shape):
    """Kreiranje pomaka"""
    initial = tf.zeros(shape, dtype=tf.float32)
    return tf.Variable(initial)


def variable_summaries(var, name):
    """Prikupljanje podataka za Tensorboard"""
    with tf.name_scope(name):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)
        tf.summary.scalar('max', tf.reduce_max(var))
        tf.summary.scalar('min', tf.reduce_min(var))
        tf.summary.histogram(name, var)


def vae_layer(input_tensor, input_dim, output_dim, layer_name,
              act=tf.nn.softplus):
    """Kreiranje jednog skrivenog sloja"""
    # Adding a name scope ensures logical grouping of the layers in the graph.
    with tf.name_scope(layer_name):
        # This Variable will hold the state of the weights for the layer
        weights = weight_variable([input_dim, output_dim],
                                  layer_name + '/weights')
        variable_summaries(weights, 'weights')
        tf.summary.tensor_summary('weightsT', weights)
        biases = bias_variable([output_dim])
        variable_summaries(biases, 'biases')
        preactivate = tf.matmul(input_tensor, weights) + biases
        tf.summary.histogram('pre_activations', preactivate)
        activations = act(preactivate, name='activation')
        tf.summary.histogram('activations', activations)
    return activations
