"""
"""

import numpy as np


def softmax(x):
    """

    :param x:
    :return:
    """
    exp_x_shifted = np.exp(x - np.max(x))
    return exp_x_shifted / np.sum(exp_x_shifted, axis=1).reshape(-1, 1)


def relu(x):
    """

    :param x:
    :return:
    """
    return np.maximum(0, x)


def train(X, Y_, delta=0.1, alpha=1e-3, h=5, n_iter=int(1e5), trace=False):
    """

    :param X:
    :param Y_:
    :param delta:
    :param alpha:
    :param n_iter:
    :param trace:
    :return:
    """

    C = np.max(Y_) + 1
    n_samples, n_features = X.shape

    delta /= n_samples

    W1 = np.random.randn(h, n_features)
    b1 = np.zeros(h)

    W2 = np.random.randn(C, h)
    b2 = np.zeros(C)

    for i in range(n_iter):
        S1 = np.dot(X, np.transpose(W1)) + b1  # N * h
        H1 = relu(S1)  # N * H

        S2 = np.dot(H1, np.transpose(W2)) + b2  # N * C

        probs = softmax(S2)

        correct_class_prob = probs[range(len(X)), Y_]
        correct_class_logprobs = -np.log(correct_class_prob)  # N x 1
        loss = correct_class_logprobs.sum()

        if trace and i % 10000 == 0:
            print("iteration {}: loss {}".format(i, loss))

        d_S2 = probs  # N * C
        d_S2[range(n_samples), Y_] -= 1

        d_W2 = np.dot(np.transpose(d_S2), H1)
        d_b2 = np.sum(d_S2, axis=0)

        d_H1 = np.dot(d_S2, W2)

        d_S1 = d_H1
        d_S1[S1 <= 0] = 0

        d_W1 = np.dot(np.transpose(d_S1), X)
        d_b1 = np.sum(d_S1, axis=0)

        W1 = W1 * (1 - alpha * delta) - delta * d_W1
        b1 += -delta * d_b1

        W2 = W2 * (1 - alpha * delta) - delta * d_W2
        b2 += -delta * d_b2

    return W1, b1, W2, b2


def classify(X, W1, b1, W2, b2):
    """

    :param X:
    :return:
    """
    S1 = np.dot(X, np.transpose(W1)) + b1
    H1 = relu(S1)

    S2 = np.dot(H1, np.transpose(W2)) + b2

    return softmax(S2)


def predict(probs):
    """

    :param probs:
    :return:
    """
    return np.argmax(probs, axis=1)
