"""
"""

import numpy as np
import tensorflow as tf


class TFLogreg:

    def __init__(self, n_features, n_classes, delta=0.1, alpha=1e-4):
        """

        :param n_features:
        :param n_classes:
        :param delta:
        """
        # formulation of data
        self.X = tf.placeholder(tf.float32, [None, n_features])
        self.Yoh_ = tf.placeholder(tf.float32, [None, n_classes])

        self.n_features = n_features
        self.n_classes = n_classes

        # formulation of model
        self.W = tf.Variable(tf.random_normal([n_classes, n_features]))
        self.b = tf.Variable(tf.zeros([n_classes]))

        self.probs = tf.nn.softmax(tf.matmul(self.X, self.W,
                                             transpose_b=True) + self.b)

        # formulation of loss function
        correct_class_logprobs = - tf.log(self.probs)
        loss = tf.reduce_sum(self.Yoh_ * correct_class_logprobs, 1)
        l2_loss = tf.nn.l2_loss(self.W)

        self.loss = tf.reduce_mean(loss) + alpha * l2_loss

        # formulation of optimisation
        trainer = tf.train.GradientDescentOptimizer(delta)
        self.train_step = trainer.minimize(self.loss)

        # new tf session
        self.session = tf.Session()

    def train(self, X, Yoh_, n_iter=1000, trace=False):
        """

        :param X:
        :param Y_:
        :param n_iter:
        :return:
        """
        self.session.run(tf.global_variables_initializer())

        for i in range(n_iter):
            loss, step = self.session.run([self.loss, self.train_step],
                                          feed_dict={self.X: X,
                                                     self.Yoh_: Yoh_})

            if i % 100 == 0 and trace:
                print("step %d, loss = %s" % (i, loss))

        return self

    def predict_proba(self, X):
        """

        :param X:
        :return:
        """
        probs = self.session.run(self.probs, feed_dict={self.X: X})
        return probs

    def predict(self, X):
        """

        :param X:
        :return:
        """
        return np.argmax(self.predict_proba(X), axis=1)
