"""
"""

import numpy as np
import tensorflow as tf

import matplotlib.pyplot as plt


def make_dataset(n, x_min=-10, x_max=10, mean=0, sigma=1):
    """

    :param n:
    :return:
    """
    X_sample = np.random.uniform(x_min, x_max, n)
    Y_sample = X_sample + 3 + np.random.normal(mean, sigma, n)

    return X_sample.reshape(-1, 1), Y_sample.reshape(-1, 1)


def train(X_sample, Y_sample, plot=False, trace=False):
    """

    :param X_sample:
    :param Y_sample:
    :return:
    """
    n = len(X_sample)

    X = tf.placeholder(tf.float32, [None, 1])
    Y_ = tf.placeholder(tf.float32, [None, 1])

    a = tf.Variable(0.0)
    b = tf.Variable(0.0)

    Y = a * X + b
    loss = (1 / (2 * n)) * (Y - Y_) ** 2

    trainer = tf.train.GradientDescentOptimizer(0.01)
    train_op = trainer.minimize(loss)

    gradients = trainer.compute_gradients(loss, [a, b])
    optimize = trainer.apply_gradients(gradients)
    gradients = tf.Print(gradients, [gradients], "Info:")

    d_a = (1 / n) * tf.matmul(Y - Y_, -X, transpose_a=True)
    d_b = (1 / n) * tf.reduce_sum(-Y + Y_)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        if plot:
            plt.scatter(X_sample, Y_sample, marker='o')

        val_a = None
        val_b = None

        for i in range(1000):
            val_loss, val_grads, da, db = sess.run(
                [loss, gradients, d_a, d_b],
                feed_dict={X: X_sample, Y_: Y_sample})

            sess.run(train_op, feed_dict={X: X_sample, Y_: Y_sample})
            val_a, val_b = sess.run([a, b],
                                    feed_dict={X: X_sample, Y_: Y_sample})

            if i % 100 == 0 and trace:
                print("a = %s, b = %s, loss = %s" % (val_a, val_b,
                                                     val_loss.sum()))
                print("tf-gradients: %s" % (val_grads[:, :1]))
                print("calculated gradients:", da, db, "\n")

        if plot:
            plt.plot(X_sample, val_a * X_sample + val_b, '-')
            plt.show()

        return val_a, val_b


def predict(X, a, b):
    """

    :param X:
    :param a:
    :param b:
    :return:
    """
    return a * X + b
