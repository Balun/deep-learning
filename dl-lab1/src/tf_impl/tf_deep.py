"""

"""

import tensorflow as tf
import numpy as np

from sklearn.model_selection import train_test_split


class TFDeep:
    """
    """

    def __init__(self, layers, delta=0.1, alpha=1e-4, activation=tf.nn.relu,
                 optimizer=tf.train.GradientDescentOptimizer):
        """

        :param layers:
        :param delta:
        :param alpha:
        """
        n_features = layers[0]
        n_classes = layers[-1]
        n_layers = len(layers) - 1

        self.X = tf.placeholder(tf.float32, [None, n_features])
        self.Yoh_ = tf.placeholder(tf.float32, [None, n_classes])

        self.activation = activation
        self.layers = layers
        self.delta = delta
        self.alpha = alpha

        self.W = []
        self.b = []

        activations = [activation] * (n_layers - 1) + [tf.nn.softmax]

        regulized = 0
        prev_output = self.X

        for i, (prev_layer, nex_layer) in enumerate(zip(layers, layers[1:])):
            W = tf.Variable(tf.random_normal([nex_layer, prev_layer]),
                            name='W%d' % i)
            b = tf.Variable(tf.random_normal([nex_layer]), name='b%d' % i)

            self.W.append(W)
            self.b.append(b)

            s = tf.add(tf.matmul(prev_output, W, transpose_b=True), b)

            prev_output = activations[i](s)

            regulized += tf.nn.l2_loss(W)

        self.probs = prev_output

        loss = tf.reduce_mean(
            -tf.reduce_sum(self.Yoh_ * tf.log(self.probs + 1e-10),
                           1))
        self.loss = loss + alpha * regulized

        self.step = optimizer(delta).minimize(
            self.loss)
        self.session = tf.Session()

    def train(self, X, Yoh_, n_iter=1000, trace=False):
        """

        :param X:
        :param Yoh_:
        :param n_iter:
        :param trace:
        :return:
        """
        self.session.run(tf.global_variables_initializer())

        for i in range(n_iter):
            loss, step = self.session.run([self.loss, self.step],
                                          feed_dict={self.X: X,
                                                     self.Yoh_: Yoh_})

            if i % 100 == 0 and trace:
                print("step %d, loss = %s" % (i, loss))

        return self

    def train_mb(self, X, Yoh_, epochs=100, batch_size=50, ratio=1,
                 trace=False):
        """

        :param X:
        :param Yoh_:
        :param epochs:
        :param batch_size:
        :param ratio:
        :param trace:
        :return:
        """
        self.session.run(tf.global_variables_initializer())
        prev_loss = window_loss = float('inf')

        X_train, X_val, Y_train, Y_val = train_test_split(X, Yoh_,
                                                          test_size=ratio)
        n_samples = len(X_train)
        n_batches = int(n_samples / batch_size)

        for epoch in range(epochs):
            X_train, Y_train = permutate(X_train, Y_train)
            i = 0
            avg_loss = 0

            while i < n_samples:
                batch_X, batch_Yoh_ = X_train[i:i + batch_size], Y_train[
                                                                 i:i + batch_size]
                data_dict = {self.X: batch_X, self.Yoh_: batch_Yoh_}
                val_loss, _ = self.session.run([self.loss, self.step],
                                               feed_dict=data_dict)

                avg_loss += val_loss / n_batches
                i += batch_size

            # validation
            data_dict = {self.X: X_val, self.Yoh_: Y_val}
            val_loss, _ = self.session.run([self.loss, self.step],
                                           feed_dict=data_dict)
            window_loss = min(window_loss, val_loss)
            if epoch % 50 == 0:
                if window_loss > prev_loss:
                    print("Early stopping: epoch", epoch)
                    return self

                prev_loss = window_loss
                window_loss = float('inf')

            if trace:
                print(
                    "Epoch: %d, avg_train_loss = %.5f, validation_loss = "
                    "%.5f" % (epoch, avg_loss, val_loss))

        return self

    def predict_proba(self, X):
        """

        :param X:
        :return:
        """
        return self.session.run(self.probs, feed_dict={self.X: X})

    def predict(self, X):
        """

        :param X:
        :return:
        """
        return np.argmax(self.predict_proba(X), axis=1)

    @property
    def weights(self):
        """

        :return:
        """
        return self.session.run(self.W)


def permutate(X, Yoh_):
    perm = np.random.permutation(len(X))
    return X[perm], Yoh_[perm]
