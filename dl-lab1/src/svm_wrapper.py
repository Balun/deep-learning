"""

"""

import sklearn.svm as svm


class KSVM:
    """

    """

    def __init__(self, X, Y_, c=1, gamma='auto', kernel='rbf'):
        """

        :param X:
        :param Y_:
        :param c:
        :param gamma:
        """
        self.model = svm.SVC(gamma=gamma, C=c, probability=True, kernel=kernel)
        self.model.fit(X, Y_)

    def predict(self, X):
        """

        :param X:
        :return:
        """
        return self.model.predict(X)

    def get_scores(self, X):
        """

        :param X:
        :return:
        """
        return self.model.predict_proba(X)

    @property
    def support(self):
        """

        :return:
        """
        return self.model.support_
