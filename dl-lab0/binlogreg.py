"""

"""

import numpy as np
import sklearn


def sigmoid(x, alpha=1):
    """

    :param x:
    :param alpha:
    :return:
    """
    return 1 / (1 + np.exp(-alpha * x))


def cross_entropy(probs, Y_):
    """

    :param X:
    :param y:
    :param w:
    :return:
    """
    return sklearn.metrics.log_loss(Y_, probs)


def train(X, Y_, delta=0.01, alpha=0, n_iter=1000, trace=False):
    '''
      Argumenti
        X:  podatci, np.array NxD
        Y_: indeksi razreda, np.array Nx1

      Povratne vrijednosti
        w, b: parametri logističke regresije
    '''
    n, dim = X.shape
    w = np.random.randn(dim)
    b = 0

    for i in range(n_iter):
        scores = np.dot(X, w) + b

        probs = sigmoid(scores)

        loss = cross_entropy(probs, Y_)

        if trace and i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))

        d_b = 1. / n * np.sum(probs - Y_)
        d_w = 1. / n * np.dot(probs - Y_, X)

        b += -delta * d_b
        w = w * (1 - alpha * delta) - delta * d_w

    return w, b


def classify(X, w, b):
    '''
      Argumenti
          X:    podatci, np.array NxD
          w, b: parametri logističke regresije

      Povratne vrijednosti
          probs: vjerojatnosti razreda c1
    '''
    return sigmoid(np.dot(X, w) + b)
