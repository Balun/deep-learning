"""

"""
import numpy as np
import matplotlib.pyplot as plt


class Random2DGaussian:
    """

    """

    MIN_X = 0
    MAX_X = 10
    MIN_Y = 0
    MAX_Y = 10
    SCALE = 5

    def __init__(self):
        """

        """
        self.mean = (np.random.uniform(self.MIN_X, self.MAX_X),
                     np.random.uniform(self.MIN_Y, self.MAX_Y))

        eigvals = np.diag(
            np.square([np.random.uniform(self.MIN_X, self.MAX_X / 5),
                       np.random.uniform(self.MIN_Y, self.MAX_Y / 5)]))

        self.theta = np.random.uniform(0, 2 * np.pi)

        self.rot = np.array([[np.cos(self.theta), -np.sin(self.theta)],
                             [np.sin(self.theta), np.cos(self.theta)]])

        self.cov = np.dot(np.dot(np.transpose(self.rot), eigvals), self.rot)

    def get_sample(self, n):
        """

        :param n:
        :return:
        """
        return np.random.multivariate_normal(self.mean, self.cov, n)


def sample_gauss_2d(C, N):
    """

    :param C:
    :param N:
    :return:
    """
    # create the distributions and groundtruth labels
    Gs = []
    Ys = []
    for i in range(C):
        Gs.append(Random2DGaussian())
        Ys.append(i)

    # sample the dataset
    X = np.vstack([G.get_sample(N) for G in Gs])
    Y_ = np.hstack([[Y] * N for Y in Ys])

    return X, Y_


def eval_perf_binary(Y, Y_):
    """

    :param Y:
    :param Y_:
    :return:
    """
    N = len(Y)

    tp = sum(np.logical_and(Y == Y_, Y_ == True))
    fn = sum(np.logical_and(Y != Y_, Y_ == True))
    tn = sum(np.logical_and(Y == Y_, Y_ == False))
    fp = sum(np.logical_and(Y != Y_, Y_ == False))

    acc = (tp + tn) / N
    prec = tp / (tp + fp)
    rec = tp / (tp + fn)

    return acc, prec, rec


def eval_perf_multi(Y, Y_):
    pr = []
    n = max(Y_) + 1
    M = np.bincount(n * Y_ + Y, minlength=n * n).reshape(n, n)
    for i in range(n):
        tp_i = M[i, i]
        fn_i = np.sum(M[i, :]) - tp_i
        fp_i = np.sum(M[:, i]) - tp_i
        tn_i = np.sum(M) - fp_i - fn_i - tp_i
        recall_i = tp_i / (tp_i + fn_i)
        precision_i = tp_i / (tp_i + fp_i)
        pr.append((recall_i, precision_i))

    accuracy = np.trace(M) / np.sum(M)

    return accuracy, pr, M


def eval_AP(Y_r):
    """

    :param Y_r:
    :return:
    """
    n = len(Y_r)
    pos = sum(Y_r)
    neg = n - pos

    tp = pos
    tn = 0
    fn = 0
    fp = neg

    sumprec = 0

    for x in Y_r:
        precision = tp / (tp + fp)

        if x:
            sumprec += precision

        tp -= x
        fn += x
        fp -= not x
        tn += not x

    return sumprec / pos


def graph_data(X, Y_, Y, special=[]):
    """Creates a scatter plot (visualize with plt.show)

    Arguments:
        X:       datapoints
        Y_:      groundtruth classification indices
        Y:       predicted class indices
        special: use this to emphasize some points

    Returns:
        None
    """
    # colors of the datapoint markers
    palette = ([0.5, 0.5, 0.5], [1, 1, 1], [0.2, 0.2, 0.2])
    colors = np.tile([0.0, 0.0, 0.0], (Y_.shape[0], 1))
    for i in range(len(palette)):
        colors[Y_ == i] = palette[i]

    # sizes of the datapoint markers
    sizes = np.repeat(20, len(Y_))
    sizes[special] = 40

    # draw the correctly classified datapoints
    good = (Y_ == Y)
    plt.scatter(X[good, 0], X[good, 1], c=colors[good],
                s=sizes[good], marker='o')

    # draw the incorrectly classified datapoints
    bad = (Y_ != Y)
    plt.scatter(X[bad, 0], X[bad, 1], c=colors[bad],
                s=sizes[bad], marker='s')


def graph_surface(function, rect, offset=0.5, width=256, height=256):
    """Creates a surface plot (visualize with plt.show)

    Arguments:
      function: surface to be plotted
      rect:     function domain provided as:
                ([x_min,y_min], [x_max,y_max])
      offset:   the level plotted as a contour plot

    Returns:
      None
    """

    lsw = np.linspace(rect[0][1], rect[1][1], width)
    lsh = np.linspace(rect[0][0], rect[1][0], height)
    xx0, xx1 = np.meshgrid(lsh, lsw)
    grid = np.stack((xx0.flatten(), xx1.flatten()), axis=1)

    # get the values and reshape them
    values = function(grid).reshape((width, height))

    # fix the range and offset
    delta = offset if offset else 0
    maxval = max(np.max(values) - delta, - (np.min(values) - delta))

    # draw the surface and the offset
    plt.pcolormesh(xx0, xx1, values,
                   vmin=delta - maxval, vmax=delta + maxval)

    if offset != None:
        plt.contour(xx0, xx1, values, colors='black', levels=[offset])
