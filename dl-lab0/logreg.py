"""

"""

import numpy as np
import sklearn


def softmax(x):
    """

    :param x:
    :return:
    """
    exp_x_shifted = np.exp(x - np.max(x))
    return exp_x_shifted / np.sum(exp_x_shifted)


def cross_entropy(probs, Y_):
    """

    :param X:
    :param y:
    :param w:
    :return:
    """
    return sklearn.metrics.log_loss(Y_, probs)


def train(X, Y_, delta=0.01, alpha=0, n_iter=1000, trace=False):
    '''
      Argumenti
        X:  podatci, np.array NxD
        Y_: indeksi razreda, np.array Nx1

      Povratne vrijednosti
        w, b: parametri logističke regresije
    '''
    C = np.max(Y_) + 1
    n_samples, n_features = X.shape
    W = np.random.randn(C, n_features)
    b = np.zeros(C)

    for i in range(n_iter):

        # eksponencirani klasifikacijski rezultati
        scores = np.dot(X, W.T) + b  # N x C
        expscores = np.exp(scores)  # N x C

        # nazivnik sofmaksa
        sumexp = expscores.sum(axis=1)  # N x 1

        # logaritmirane vjerojatnosti razreda
        probs = expscores / sumexp.reshape(-1, 1)  # N x C
        correct_class_prob = probs[range(len(X)), Y_]

        correct_class_logprobs = -np.log(correct_class_prob)  # N*1
        # gubitak
        loss = correct_class_logprobs.sum()

        # dijagnostički ispis
        if trace and i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))

        # derivacije komponenata gubitka po rezultatu
        dL_ds = probs  # N x C
        dL_ds[range(len(X)), Y_] -= 1

        # print('dl_ds', dL_ds.shape)

        # gradijenti parametara
        grad_W = 1.0 / n_samples * np.dot(dL_ds.T, X)  # C x D
        grad_b = 1.0 / n_samples * dL_ds.sum(axis=0)  # C x 1

        W += -delta * grad_W
        b += -delta * grad_b

    return W, b


def classify(X, W, b):
    """

    :param X:
    :param W:
    :param b:
    :return:
    """
    return softmax(np.dot(X, np.transpose(W)) + b)


def predict(probs):
    """

    :param probs:
    :return:
    """
    return np.argmax(probs, axis=1)
