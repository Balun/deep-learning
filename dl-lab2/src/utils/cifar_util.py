"""

"""

import numpy as np
import skimage as ski
import skimage.io
import matplotlib.pyplot as plt
import tensorflow as tf

from tensorflow.contrib.layers import \
    xavier_initializer_conv2d as xavier_conv2d
from tensorflow.contrib.layers import xavier_initializer as xavier

from time import strftime

import pickle

import os
import math

DATA_DIR = 'res/data/CIFAR'
SAVE_DIR = "res/plots/CIFAR"


def shuffle_data(data_x, data_y):
    """

    :param data_x:
    :param data_y:
    :return:
    """
    indices = np.arange(data_x.shape[0])
    np.random.shuffle(indices)
    shuffled_data_x = np.ascontiguousarray(data_x[indices])
    shuffled_data_y = np.ascontiguousarray(data_y[indices])
    return shuffled_data_x, shuffled_data_y


def unpickle(file):
    """

    :param file:
    :return:
    """
    fo = open(file, 'rb')
    dict = pickle.load(fo, encoding='latin1')
    fo.close()
    return dict


def load_data():
    """

    :return:
    """
    img_height, img_width = 32, 32
    num_channels = 3

    train_x = np.ndarray((0, img_height * img_width * num_channels),
                         dtype=np.float32)
    train_y = []
    for i in range(1, 6):
        subset = unpickle(os.path.join(DATA_DIR, 'data_batch_%d' % i))
        train_x = np.vstack((train_x, subset['data']))
        train_y += subset['labels']
    train_x = train_x.reshape(
        (-1, num_channels, img_height, img_width)).transpose(0, 2, 3, 1)
    train_y = np.array(train_y, dtype=np.int32)

    subset = unpickle(os.path.join(DATA_DIR, 'test_batch'))
    test_x = subset['data'].reshape(
        (-1, num_channels, img_height, img_width)).transpose(0, 2, 3,
                                                             1).astype(
        np.float32)
    test_y = np.array(subset['labels'], dtype=np.int32)

    valid_size = 5000
    train_x, train_y = shuffle_data(train_x, train_y)
    valid_x = train_x[:valid_size, ...]
    valid_y = train_y[:valid_size, ...]
    train_x = train_x[valid_size:, ...]
    train_y = train_y[valid_size:, ...]
    data_mean = train_x.mean((0, 1, 2))
    data_std = train_x.std((0, 1, 2))

    train_x = (train_x - data_mean) / data_std
    valid_x = (valid_x - data_mean) / data_std
    test_x = (test_x - data_mean) / data_std

    return train_x, train_y, valid_x, valid_y, test_x, test_y


def draw_conv_filters(epoch, step, weights, save_dir):
    w = weights.copy()
    num_filters = w.shape[3]
    num_channels = w.shape[2]
    k = w.shape[0]

    assert w.shape[0] == w.shape[1]

    w = w.reshape(k, k, num_channels, num_filters)
    w -= w.min()
    w /= w.max()
    border = 1

    cols = 8
    rows = math.ceil(num_filters / cols)

    width = cols * k + (cols - 1) * border
    height = rows * k + (rows - 1) * border

    img = np.zeros([height, width, num_channels])

    for i in range(num_filters):
        r = int(i / cols) * (k + border)
        c = int(i % cols) * (k + border)
        img[r:r + k, c:c + k, :] = w[:, :, :, i]

    filename = 'epoch_%02d_step_%06d.png' % (epoch, step)
    ski.io.imsave(os.path.join(save_dir, filename), img)


def draw_image(img, mean, std):
    img *= std
    img += mean
    img = img.astype(np.uint8)
    ski.io.imshow(img)
    ski.io.show()


def save_image(img, path, mean, std):
    img = img.copy()
    img *= std
    img += mean
    img = img.astype(np.uint8)
    ski.io.imsave(path, img)


def plot_training_progress(data, save_dir=SAVE_DIR):
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(16, 8))

    linewidth = 2
    legend_size = 10
    train_color = 'm'
    val_color = 'c'

    num_points = len(data['train_loss'])
    x_data = np.linspace(1, num_points, num_points)
    ax1.set_title('Cross-entropy loss')
    ax1.plot(x_data, data['train_loss'], marker='o', color=train_color,
             linewidth=linewidth, linestyle='-', label='train')
    ax1.plot(x_data, data['valid_loss'], marker='o', color=val_color,
             linewidth=linewidth, linestyle='-', label='validation')
    ax1.legend(loc='upper right', fontsize=legend_size)
    ax2.set_title('Average class accuracy')
    ax2.plot(x_data, data['train_acc'], marker='o', color=train_color,
             linewidth=linewidth, linestyle='-', label='train')
    ax2.plot(x_data, data['valid_acc'], marker='o', color=val_color,
             linewidth=linewidth, linestyle='-', label='validation')
    ax2.legend(loc='upper left', fontsize=legend_size)
    ax3.set_title('Learning rate')
    ax3.plot(x_data, data['lr'], marker='o', color=train_color,
             linewidth=linewidth, linestyle='-', label='learning_rate')
    ax3.legend(loc='upper left', fontsize=legend_size)

    save_path = os.path.join(save_dir, 'training_plot.pdf')
    print('Plotting in: ', save_path)
    plt.savefig(save_path)


def l2_loss(weights):
    regularizers = 0
    for w in weights:
        regularizers += tf.nn.l2_loss(w)
    return regularizers


def convolution_2d(x, W, b, activation=tf.nn.relu, strides=1):
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
    x = tf.nn.bias_add(x, b)
    return activation(x)


def maxpool_2d(x, k=2, stride=2):
    return tf.nn.max_pool(x, ksize=[1, k, k, 1],
                          strides=[1, stride, stride, 1], padding='SAME')


def dense(x, W, b, activation=None):
    x = tf.reshape(x, [-1, W.get_shape().as_list()[0]])
    if activation:
        return activation(tf.matmul(x, W) + b)
    return tf.matmul(x, W) + b


def get_weights_and_biases():
    n_classes = 10

    weights = {
        'conv1': tf.get_variable('w_conv1', [5, 5, 3, 16],
                                 initializer=xavier_conv2d()),
        'conv2': tf.get_variable('w_conv2', [5, 5, 16, 32],
                                 initializer=xavier_conv2d()),

        'fc3': tf.get_variable('w_fc3', [8 * 8 * 32, 256],
                               initializer=xavier()),
        'fc4': tf.get_variable('w_fc4', [256, 128], initializer=xavier()),
        'fc5': tf.get_variable('w_fc5', [128, n_classes], initializer=xavier())

    }

    biases = {
        'conv1': tf.Variable(tf.zeros([16]), name='b_conv1'),
        'conv2': tf.Variable(tf.zeros([32]), name='b_conv2'),

        'fc3': tf.Variable(tf.zeros([256]), name='b_fc3'),
        'fc4': tf.Variable(tf.zeros([128]), name='b_fc4'),
        'fc5': tf.Variable(tf.zeros([n_classes]), name='b_fc5')
    }

    return weights, biases


def create_model(X, weights, biases):
    img_height, img_width, num_channels = 32, 32, 3

    x = tf.reshape(X, shape=[-1, img_height, img_width, num_channels])

    model = convolution_2d(x, weights['conv1'], biases['conv1'], tf.nn.relu)
    model = maxpool_2d(model, k=3, stride=2)

    model = convolution_2d(model, weights['conv2'], biases['conv2'],
                           tf.nn.relu)
    model = maxpool_2d(model, k=3, stride=2)

    model = dense(model, weights['fc3'], biases['fc3'], tf.nn.relu)
    model = dense(model, weights['fc4'], biases['fc4'], tf.nn.relu)
    model = dense(model, weights['fc5'], biases['fc5'])

    return model


def save_model(saver, path, session):
    save_path = saver.save(session, path)
    print("Model saved in file: %s" % save_path)


def restore_model(saver, session, path):
    saver.restore(session, path)
    print("Model restored.")
