"""

"""

import tensorflow as tf
import numpy as np
import skimage.io

import os
import math

from tensorflow.examples.tutorials.mnist import input_data

DATA_DIR = "res/data/MNIST"


def load_train_data(data_dir=DATA_DIR):
    """

    :return:
    """
    dataset = input_data.read_data_sets(data_dir, one_hot=True)

    train_x = dataset.train.images
    train_x = train_x.reshape([-1, 28, 28, 1])
    train_y = dataset.train.labels

    train_mean = train_x.mean()
    train_x -= train_mean

    return train_x, train_y, train_mean


def load_test_data(train_mean, data_dir=DATA_DIR):
    """

    :param train_mean:
    :return:
    """
    dataset = input_data.read_data_sets(data_dir, one_hot=True)

    test_x = dataset.test.images
    test_x = test_x.reshape([-1, 28, 28, 1])
    test_y = dataset.test.labels

    test_x -= train_mean

    return test_x, test_y


def load_validation_data(train_mean, data_dir=DATA_DIR):
    """

    :param train_mean:
    :return:
    """
    dataset = input_data.read_data_sets(data_dir, one_hot=True)

    valid_x = dataset.validation.images
    valid_x = valid_x.reshape([-1, 28, 28, 1])
    valid_y = dataset.validation.labels

    valid_x -= train_mean

    return valid_x, valid_y


def initialize_variables(shape, fin):
    """

    :param shape:
    :param fin:
    :return:
    """
    sigma = np.sqrt(2 / fin)
    return tf.Variable(tf.truncated_normal(shape, stddev=sigma))


def create_model(X, weights, biases):
    x = tf.reshape(X, shape=[-1, 28, 28, 1])

    model = convolution_2d(x, weights['conv1'], biases['conv1'], tf.nn.relu)
    model = maxpool_2d(model, k=2)

    model = convolution_2d(model, weights['conv2'], biases['conv2'],
                           tf.nn.relu)
    model = maxpool_2d(model, k=2)

    model = dense(model, weights['fc1'], biases['fc1'], tf.nn.relu)
    model = dense(model, weights['fc2'], biases['fc2'])

    return model


def get_weights_and_biases():
    """

    :return:
    """
    weights = {
        'conv1': initialize_variables([5, 5, 1, 16], 5 * 5),
        # 5x5 conv, 1 input, 16 outputs
        'conv2': initialize_variables([5, 5, 16, 32], 5 * 5 * 16),
        # 5x5 conv, 16 inputs, 32 outputs

        'fc1': initialize_variables([7 * 7 * 32, 512], 7 * 7 * 32),
        # fully connected, 7*7*32 inputs, 512 outputs
        'fc2': initialize_variables([512, 10], 512)
        # 512 inputs, 10 outputs (class prediction)
    }

    biases = {
        'conv1': tf.Variable(tf.zeros([16])),
        'conv2': tf.Variable(tf.zeros([32])),
        'fc1': tf.Variable(tf.zeros([512])),
        'fc2': tf.Variable(tf.zeros([10]))
    }

    return weights, biases


def convolution_2d(x, W, b, activation=tf.nn.relu, strides=1):
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
    x = tf.nn.bias_add(x, b)
    return activation(x)


def maxpool_2d(x, k=2):
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1],
                          padding='SAME')


def dense(x, W, b, activation=None):
    x = tf.reshape(x, [-1, W.get_shape().as_list()[0]])
    if activation:
        return activation(tf.matmul(x, W) + b)

    return tf.matmul(x, W) + b


def l2_loss(weights):
    regularizers = 0

    for w in weights:
        regularizers += tf.nn.l2_loss(w)

    return regularizers


def draw_conv_filters(epoch, step, name, weights, save_dir):
    # kxkxCxn_filters
    k, k, C, num_filters = weights.shape

    w = weights.copy().swapaxes(0, 3).swapaxes(1, 2)
    w = w.reshape(num_filters, C, k, k)
    w -= w.min()
    w /= w.max()

    border = 1
    cols = 8
    rows = math.ceil(num_filters / cols)
    width = cols * k + (cols - 1) * border
    height = rows * k + (rows - 1) * border

    for i in range(1):
        img = np.zeros([height, width])
        for j in range(num_filters):
            r = int(j / cols) * (k + border)
            c = int(j % cols) * (k + border)
            img[r:r + k, c:c + k] = w[j, i]
        filename = '%s_epoch_%02d_step_%06d_input_%03d.png' % (
            name, epoch, step, i)
        skimage.io.imsave(os.path.join(save_dir, filename), img)
