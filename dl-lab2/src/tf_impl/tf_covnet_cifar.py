"""

"""
import tensorflow as tf
import numpy as np
import time

import src.utils.cifar_util as utils

SAVE_DIR = 'res/output/CIFAR'


class TFCovnetCIFAR:
    """

    """

    def __init__(self, alpha=1e-3):
        """

        :param alpha:
        """
        self.n_features = 1024
        self.n_classes = 10

        self.X = tf.placeholder(tf.float32, [None, 32, 32, 3])
        self.Y_ = tf.placeholder(tf.int32, [None, ])

        self.weights, self.biases = utils.get_weights_and_biases()

        self.logits = utils.create_model(self.X, self.weights, self.biases)

        regularizers = utils.l2_loss(self.weights.values())
        self.loss_per_sample = tf.nn.sparse_softmax_cross_entropy_with_logits(
            logits=self.logits, labels=self.Y_)
        data_loss = tf.reduce_mean(self.loss_per_sample)

        self.loss = data_loss + alpha * regularizers

        global_step = tf.Variable(0, trainable=False)

        self.learning_rate = tf.train.exponential_decay(0.01, global_step,
                                                        900, 0.9,
                                                        staircase=True)
        self.train_step = tf.train.GradientDescentOptimizer(
            self.learning_rate).minimize(
            self.loss, global_step=global_step)

        self.session = tf.Session()

    def fit(self, X_train, Y_train_, X_val, Y_val_, batch_size=50, epochs=1,
            trace=False):
        """

        :param X:
        :param Y_:
        :param batch_size:
        :param epochs:
        :param trace:
        :return:
        """
        plot_data = {}
        plot_data['train_loss'] = []
        plot_data['valid_loss'] = []
        plot_data['train_acc'] = []
        plot_data['valid_acc'] = []
        plot_data['lr'] = []

        self.session.run(tf.initialize_all_variables())
        num_examples = X_train.shape[0]

        assert num_examples % batch_size == 0

        num_batches = num_examples // batch_size

        for epoch_num in range(1, epochs + 1):
            epoch_start = time.time()
            X_train, Y_train_ = utils.shuffle_data(X_train, Y_train_)

            for step in range(num_batches):
                offset = step * batch_size

                batch_x = X_train[offset:(offset + batch_size), ...]
                batch_y = Y_train_[offset:(offset + batch_size), ...]

                feed_dict = {self.X: batch_x, self.Y_: batch_y}
                start_time = time.time()
                ret_val = self.session.run([self.train_step, self.loss,
                                            self.logits],
                                           feed_dict=feed_dict)
                _, loss_val, logits_val = ret_val
                duration = time.time() - start_time

                if (step + 1) % 50 == 0 and trace:
                    sec_per_batch = float(duration)
                    format_str = 'epoch %d, step %d / %d, loss = %.2f (%.3f sec/batch)'
                    print(format_str % (
                        epoch_num, step + 1, num_batches, loss_val,
                        sec_per_batch))

                if (step + 1) % 100 == 0 and trace:
                    w = self.session.run(self.weights['conv1'])
                    utils.draw_conv_filters(epoch_num, step + 1, w, SAVE_DIR)

            print('Train error:')
            train_loss, train_acc = self.evaluate(X_train, Y_train_, batch_size)
            print('Validation error:')
            valid_loss, valid_acc = self.evaluate(X_val, Y_val_,
                                                  batch_size)
            print('Epoch time:', time.time() - epoch_start)
            plot_data['train_loss'] += [train_loss]
            plot_data['valid_loss'] += [valid_loss]
            plot_data['train_acc'] += [train_acc]
            plot_data['valid_acc'] += [valid_acc]
            plot_data['lr'] += [self.session.run(self.learning_rate)]

            utils.plot_training_progress(plot_data, SAVE_DIR)

        return plot_data

    def evaluate(self, X, Y_, batch_size=50):
        """

        :param X:
        :param Y_:
        :param batch_size:
        :return:
        """
        num_examples = X.shape[0]
        assert num_examples % batch_size == 0
        num_batches = num_examples // batch_size
        cnt_correct = 0
        loss_avg = 0

        for i in range(num_batches):
            batch_x = X[i * batch_size:(i + 1) * batch_size, ...]
            batch_y = Y_[i * batch_size:(i + 1) * batch_size, ...]

            data_dict = {self.X: batch_x, self.Y_: batch_y}
            logits_val, loss_val = self.session.run([self.logits, self.loss],
                                                    feed_dict=data_dict)

            yp = np.argmax(logits_val, 1)
            yt = batch_y
            cnt_correct += (yp == yt).sum()

            loss_avg += loss_val
        valid_acc = cnt_correct / num_examples * 100
        loss_avg /= num_batches

        print(" accuracy = %.2f" % valid_acc)
        print(" avg loss = %.2f\n" % loss_avg)

        return loss_avg, valid_acc

    def worst_samples(self, X, Y_, batch_size=50):
        num_examples = X.shape[0]
        num_batches = num_examples // batch_size

        worst_samples = []
        for i in range(num_batches):
            batch_x = X[i * batch_size:(i + 1) * batch_size, ...]
            batch_y = Y_[i * batch_size:(i + 1) * batch_size, ...]

            data_dict = {self.X: batch_x, self.Y_: batch_y}
            loss_vals, logits_val = self.session.run([self.loss_per_sample,
                                                      self.logits],
                                                     feed_dict=data_dict)
            prediction = np.argmax(logits_val, 1)
            loss_pairs = [(i * batch_size + id, loss, p) for id, (loss, p) in
                          enumerate(zip(loss_vals, prediction))]
            worst_samples = sorted(loss_pairs + worst_samples,
                                   key=lambda x: -x[1])[:20]
        return worst_samples
