import numpy as np
import tensorflow as tf

import src.utils.mnist_util as utils

DATA_DIR = "res/data/MNIST"
SAVE_DIR = "res/output/MNIST"

config = {}
config['max_epochs'] = 8
config['batch_size'] = 50
config['save_dir'] = SAVE_DIR
config['weight_decay'] = 1e-4
config['lr_policy'] = {1: {'lr': 1e-1}, 3: {'lr': 1e-2}, 5: {'lr': 1e-3},
                       7: {'lr': 1e-4}}


class TFCovnetMNIST:
    """

    """

    def __init__(self, alpha=1e-3):
        """

        :param alpha:
        """
        self.n_features = 768
        self.n_classes = 10

        self.X = tf.placeholder(tf.float32, [None, 28, 28, 1])
        self.Yoh_ = tf.placeholder(tf.float32, [None, self.n_classes])

        self.weights, self.biases = utils.get_weights_and_biases()

        self.logits = utils.create_model(self.X, self.weights, self.biases)

        # loss
        regularizers = utils.l2_loss(
            [self.weights['conv1'], self.weights['conv2'], self.weights[
                'fc1']])

        data_loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(logits=self.logits,
                                                    labels=self.Yoh_))

        self.loss = data_loss + alpha * regularizers

        self.lr = tf.placeholder(tf.float32)
        self.train_step = tf.train.GradientDescentOptimizer(self.lr).minimize(
            self.loss)
        self.session = tf.Session()

    def fit(self, X, Y_, batch_size=50, epochs=1, trace=False):
        """

        :param X:
        :param Y_:
        :return:
        """
        self.session.run(tf.global_variables_initializer())

        losses = []

        lr_policy = config['lr_policy']
        save_dir = config['save_dir']

        n_samples = X.shape[0]
        assert n_samples % batch_size == 0
        num_batches = n_samples // batch_size

        for epoch in range(1, epochs + 1):
            if epoch in lr_policy:
                solver_config = lr_policy[epoch]
            cnt_correct = 0

            permutation_idx = np.random.permutation(n_samples)
            X = X[permutation_idx]
            train_y = Y_[permutation_idx]

            for i in range(num_batches):
                # store mini-batch to ndarray
                batch_x = X[i * batch_size:(i + 1) * batch_size, :]
                batch_y = train_y[i * batch_size:(i + 1) * batch_size, :]

                data_dict = {self.X: batch_x, self.Yoh_: batch_y,
                             self.lr: solver_config['lr']}
                logits_val, loss_val, _ = self.session.run(
                    [self.logits, self.loss, self.train_step],
                    feed_dict=data_dict)

                # compute classification accuracy
                yp = np.argmax(logits_val, 1)
                yt = np.argmax(batch_y, 1)
                cnt_correct += (yp == yt).sum()

                if i % 5 == 0 and trace:
                    print("epoch %d, step %d/%d, batch loss = %.2f" % (
                        epoch, i * batch_size, n_samples, loss_val))

                if i % 100 == 0:
                    w = self.session.run(self.weights['conv1'])
                    utils.draw_conv_filters(epoch, i * batch_size, "conv1", w,
                                            save_dir)

                if i > 0 and i % 50 == 0:
                    losses.append(loss_val
                                  )
                    if trace:
                        print("Train accuracy = %.2f" % (
                                cnt_correct / ((i + 1) * batch_size) * 100))

            print("Train accuracy = %.2f" % (cnt_correct / n_samples * 100))

        return self, losses

    def evaluate(self, X, Y_, batch_size=50):
        """

        :param X:
        :param Y_:
        :return:
        """
        print("\nValidation:")

        batch_size = batch_size
        num_examples = X.shape[0]

        assert num_examples % batch_size == 0

        num_batches = num_examples // batch_size
        cnt_correct = 0
        loss_avg = 0

        for i in range(num_batches):
            batch_x = X[i * batch_size:(i + 1) * batch_size, :]
            batch_y = Y_[i * batch_size:(i + 1) * batch_size, :]

            data_dict = {self.X: batch_x, self.Yoh_: batch_y}
            logits_val, loss_val = self.session.run([self.logits, self.loss],
                                                    feed_dict=data_dict)

            yp = np.argmax(logits_val, 1)
            yt = np.argmax(batch_y, 1)
            cnt_correct += (yp == yt).sum()

            loss_avg += loss_val

        valid_acc = cnt_correct / num_examples * 100
        loss_avg /= num_batches

        print("Validation accuracy = %.2f" % valid_acc)
        print("Validation average loss = %.2f\n" % loss_avg)


def train_example():
    """

    :return:
    """

    X_train, Y_train_, mean = utils.load_train_data(DATA_DIR)
    X_val, Y_val_ = utils.load_validation_data(DATA_DIR, mean)

    alpha = config['weight_decay']

    model = TFCovnetMNIST(alpha)
    model.fit(X_train, Y_train_)
    model.evaluate(X_val, Y_val_)


if __name__ == '__main__':
    train_example()
