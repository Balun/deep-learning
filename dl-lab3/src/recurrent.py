"""

"""
import pickle
import os
import numpy as np
import sklearn.metrics as metrics

import src.utils.dataset as data_utils


class RecurrentNet:
    """

    """

    def __init__(self, hidden_size, sequence_length, vocab_size,
                 learning_rate):
        """

        :param hidden_size:
        :param sequence_length:
        :param vocab_size:
        :param learning_rate:
        """
        self.hidden_size = hidden_size
        self.sequence_length = sequence_length
        self.vocab_size = vocab_size
        self.learning_rate = learning_rate

        self.U = np.random.normal(size=[vocab_size, hidden_size],
                                  scale=1 / np.sqrt(hidden_size))
        self.W = np.random.normal(size=[hidden_size, hidden_size],
                                  scale=1 / np.sqrt(hidden_size))
        self.b = np.zeros([1, hidden_size])

        self.V = np.random.normal(size=[hidden_size, vocab_size],
                                  scale=1.0 / np.sqrt(vocab_size))
        self.c = np.zeros([1, vocab_size])

        self.memory_U, self.memory_W, self.memory_V = np.zeros_like(
            self.U), np.zeros_like(self.W), np.zeros_like(self.V)
        self.memory_b, self.memory_c = np.zeros_like(self.b), np.zeros_like(
            self.c)

    def step_forward(self, x, h_prev, U=None, W=None, b=None):
        """

        :param x:
        :param h_prev:
        :param U:
        :param W:
        :param b:
        :return:
        """
        U = U if U is not None else self.U
        W = W if W is not None else self.W
        b = b if b is not None else self.b

        if h_prev.shape[0] == 1:
            h_prev = np.broadcast_to(h_prev, (x.shape[0], h_prev.shape[1]))

        h_current = np.tanh(np.dot(h_prev, W) + np.dot(x, U) + b)

        return h_current, (h_current, h_prev, x)

    def forward(self, x, h_0, U=None, W=None, b=None):
        """

        :param x:
        :param h_0:
        :param U:
        :param W:
        :param b:
        :return:
        """
        U = U if U is not None else self.U
        W = W if W is not None else self.W
        b = b if b is not None else self.b

        x = x.transpose((1, 0, 2))
        h = [h_0]
        cache = []

        for minibatch in x:
            h_minibatch, cache_minibatch = self.step_forward(minibatch,
                                                             h[-1], U, W, b)

            cache.append(cache_minibatch)
            h.append(h_minibatch)

        return np.array(h[1:]).transpose((1, 0, 2)), cache

    def step_backward(self, grad_next, cache, d_U, d_W, d_b):
        """

        :param grad_next:
        :param cache:
        :return:
        """
        h, h_prev, x = cache
        step = grad_next * (1 - h ** 2)
        dh_prev = np.dot(step, self.W.T)
        d_W += np.dot(h_prev.T, step) / grad_next.shape[0]
        d_U += np.dot(x.T, step) / grad_next.shape[0]
        d_b += np.sum(step, axis=0) / grad_next.shape[0]

        return dh_prev

    def backward(self, d_h, cache, U=None, W=None, b=None):
        """

        :param dh:
        :param cache:
        :return:
        """
        U = U if U is not None else self.U
        W = W if W is not None else self.W
        b = b if b is not None else self.b

        d_U, d_W, d_b = np.zeros_like(U), np.zeros_like(W), np.zeros_like(b)

        d_h = d_h.transpose(1, 0, 2)

        grad = np.zeros_like(d_h[-1])
        for d_h_elem, cache_elem in reversed(list(zip(d_h, cache))):
            grad, d_U, d_W, d_b = self.step_backward(grad + d_h_elem,
                                                     cache_elem, d_U, d_W,
                                                     d_b)

        return np.clip(d_U, -5, 5), np.clip(d_W, -5, 5), np.clip(d_b, -5, 5)

    def output(self, h, V=None, c=None):
        """

        :param h:
        :param V:
        :param c:
        :return:
        """
        V = V if V else self.V
        c = c if c else self.c
        probs = []

        for h_i in h:
            h_i = np.expand_dims(h_i, axis=1)
            o = np.dot(h_i, V)

            probs.append(np.exp(np.transpose(o)) / np.sum(np.exp()))

        return np.array(probs)

    def output_loss_and_grads(self, h, y, V=None, c=None):
        """

        :param h:
        :param y:
        :param V:
        :param c:
        :return:
        """
        #   y[batch_id][timestep] = np.zeros((vocabulary_size, 1))
        #   y[batch_id][timestep][batch_y[timestep]] = 1

        #     where y might be a list or a dictionary.

        # calculate the output (o) - unnormalized log probabilities of classes
        # calculate yhat - softmax of the output
        # calculate the cross-entropy loss
        # calculate the derivative of the cross-entropy softmax loss with respect to the output (o)
        # calculate the gradients with respect to the output parameters V and c
        # calculate the gradients with respect to the hidden layer h

        V = V if V else self.V
        c = c if c else self.c

        y_pred = self.output(h, V, c)

        loss = metrics.log_loss(y.reshape(-1, self.vocab_size),
                                y_pred.reshape(-1,
                                               self.vocab_size)) * self.sequence_length

        d_o = y_pred - y
        d_V = np.zeros_like(V)
        d_c = np.zeros_like(c)
        d_h = []

        for d_o_elem, h_elem in zip(np.transpose(d_o, (1, 0, 2)),
                                    np.transpose(h, (1, 0, 2))):
            d_V += np.dot(np.transpose(h_elem), d_o_elem) / h.shape[0]
            d_c += np.average(d_o_elem, axis=0)
            d_h.append(np.dot(d_o_elem, np.transpose(V)))

        d_h = np.transpose(np.array(d_h), (1, 0, 2))

        return loss, d_h, np.clip(d_V, -5, 5), np.clip(d_c, -5, 5)

    def update(self, d_U, d_W, d_b, d_V, d_c):
        """

        :param d_U:
        :param d_W:
        :param d_b:
        :param d_V:
        :param d_c:
        :return:
        """
        delta = 1e-7
        # update memory matrices
        self.memory_U += d_U ** 2
        self.memory_W += d_W ** 2
        self.memory_b += d_b ** 2
        self.memory_V += d_V ** 2
        self.memory_c += d_c ** 2
        # perform the Adagrad update of parameters
        self.U -= self.learning_rate / (delta + np.sqrt(self.memory_U)) * d_U
        self.W -= self.learning_rate / (delta + np.sqrt(self.memory_W)) * d_W
        self.b -= self.learning_rate / (delta + np.sqrt(self.memory_b)) * d_b
        self.V -= self.learning_rate / (delta + np.sqrt(self.memory_V)) * d_V
        self.c -= self.learning_rate / (delta + np.sqrt(self.memory_c)) * d_c

    def step(self, h_0, x_oh, y_oh, ):
        """

        :param h_0:
        :param x_oh:
        :param y_oh:
        :return:
        """
        h, cache = self.forward(x_oh, h_0)
        loss, d_h, d_V, d_c = self.output_loss_and_grads(h, y_oh)
        d_U, d_W, d_b = self.backward(d_h, cache)

        self.update(d_U, d_W, d_b, d_V, d_c)

        return loss, h[:, -1, :]


def sample(seed, n_sample, model):
    h = np.zeros((1, model.hidden_size))
    seed_oh = data_utils.one_hot(seed, model.vocab_size)

    for char in seed_oh:
        # print(char.shape, char[np.newaxis, :].shape, h.shape)
        h, _ = model.rnn_step_forward(char[np.newaxis, :], h)

    sample = np.zeros((n_sample,), dtype=np.int32)
    sample[:len(seed)] = seed
    for i in range(len(seed), n_sample):
        model_out = model.output(h[np.newaxis, :, :])
        sample[i] = np.random.choice(np.arange(model_out.shape[-1]),
                                     p=model_out.ravel())

        model_out[:] = 0
        model_out = model_out.reshape(1, -1)
        model_out[0, sample[i]] = 1
        h, _ = model.rnn_step_forward(model_out, h)

    return sample


def run_language_model(
        max_epochs,
        hidden_size=100,
        sequence_length=30,
        learning_rate=1e-1,
        sample_every=100
):
    ds = data_utils.Dataset(
        batch_size=32,
        sequence_length=sequence_length
    )

    ds.preprocess(os.path.join("../res/data", 'selected_conversations.txt'))
    vocab_size = ds.vocabulary_size
    RNN = RecurrentNet(hidden_size, sequence_length, vocab_size, learning_rate)

    current_epoch = 0
    batch = 0

    h0 = np.zeros((1, hidden_size))

    average_loss = 0

    save_path = "model_data"
    if os.path.exists(save_path):
        with open(save_path, "rb") as f:
            batch, current_epoch, ds.batch_iter, RNN = pickle.load(f)
        print("===== Reading datamodel =====")

    while current_epoch < max_epochs:
        e, x, y = ds.next_minibatch()

        if e:
            current_epoch += 1
            h0 = np.zeros((1, hidden_size))
            # why do we reset the hidden state here?

        loss, h0 = RNN.step(h0, data_utils.one_hot(x, vocab_size),
                            data_utils.one_hot(y,
                                               vocab_size))
        average_loss = 0.9 * average_loss + 0.1 * loss

        if batch % sample_every == 0:
            smp = sample(ds.encode("What is the meaning of life???\n\n"), 200,
                         RNN)
            print("=====", average_loss, " EPOH", current_epoch, "BATCH",
                  batch, "=====")
            print(ds.decode(smp))

        if batch % 1000 == 0:
            print("===== Saving datamodel =====")
            with open(save_path, "wb") as f:
                pickle.dump((batch, current_epoch, ds.batch_iter, RNN), f,
                            protocol=4)
        batch += 1


if __name__ == "__main__":
    run_language_model(5)
