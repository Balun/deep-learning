"""

"""
import numpy as np

from collections import Counter


class Dataset:
    """

    """

    def __init__(self, sequence_length, file_path=None, batch_size=1):
        """

        :param file_path:
        :param data_length:
        :param batch_size:
        """
        self.file_path = file_path
        self.sequence_length = sequence_length
        self.batch_size = batch_size

        self.char_to_id = {}
        self.id_to_char = {}
        self.x = []
        self.batch_index = 0
        self.batches = None

    def preprocess(self, input_file):
        """

        :param input_file:
        :return:
        """
        with open(input_file, "r") as f:
            data = f.read()
            counter = Counter(data)

            self.char_to_id = {c[0]: i for i, c in enumerate(
                counter.most_common())}

            self.vocabulary_size = len(self.char_to_id)

            self.id_to_char = {k: v for v, k in self.char_to_id.items()}

            self.x = np.array(list(map(self.char_to_id.get, data)))

        return self.x

    def encode(self, sequence):
        """

        :param sequence:
        :return:
        """
        return np.array(list(map(self.char_to_id.get, sequence)))

    def decode(self, encoded_sequence):
        """

        :param encoded_sequence:
        :return:
        """
        return np.array(list(map(self.id_to_char.get, encoded_sequence)))

    def create_minibatches(self):
        self.batches = np.zeros((self.num_batches, self.batch_size,
                                 self.sequence_length + 1))

        for b in range(self.num_batches):
            for s in range(self.batch_size):
                sentance_start = s * (self.num_batches * self.sequence_length)
                start = b * self.sequence_length + sentance_start
                end = start + self.sequence_length + 1
                self.batches[b, s, :] = self.x[start:end]

        return self.batches

    def next_minibatch(self):
        new_epoch = self.batch_index == self.num_batches
        if new_epoch:
            self.batch_index = 0

        batch = self.batches[self.batch_index, :, :]
        self.batch_index += 1

        batch_x = batch[:, :-1]
        batch_y = batch[:, 1:]
        return new_epoch, batch_x, batch_y

    @property
    def num_batches(self):
        """

        :return:
        """
        return int(len(self.x) / (self.batch_size * self.sequence_length))

    def __iter__(self):
        """

        :return:
        """
        return self.batches

    def __next__(self):
        if self.batch_index == self.num_batches:
            self.batch_index = 0
            raise StopIteration

        else:
            batch = self.batches[self.batch_index, :, :]
            self.batch_index += 1

            batch_x = batch[:, :-1]
            batch_y = batch[:, 1:]
            return batch_x, batch_y


def one_hot(x, vocab_size):
    """
        x- > (batch_size, sequence_length)
        out -> (batch_size, sequence_length, vocab_size)
    """

    sol = np.zeros([*x.shape, vocab_size], dtype=np.float64)
    for idx in np.ndindex(*x.shape):
        sol[idx][x[idx]] = 1
    return sol
